const LCD = require("./");

new Promise((accept) => {
  accept(
    LCD.UI.initCanvas(["res/fonts/font.fnt"], ["res/img/asdf.png"], {
      width: 480,
      height: 320,
      rotation: LCD.Rotation.Down,
    })
  );
}).then((canvas) => {
  const tabs = new LCD.Tab(0, 0, 480, 320);

  const circles = new LCD.Scroll(0, 0, 480, 290);
  for (let i = 0; i < 10; i++) {
    circles.addContentChild(
      new LCD.Circle(
        Math.random() * 480,
        Math.random() * 320,
        Math.random() * 100,
        {
          backgroundColor: Math.random() * 0xffffffff,
          borderColor: Math.random() * 0xffffffff,
        }
      )
    );
  }
  tabs.addTab("Circles", circles);

  const rects = new LCD.Scroll(0, 0, 480, 290);
  for (let i = 0; i < 10; i++) {
    rects.addContentChild(
      new LCD.Rect(
        Math.random() * 480,
        Math.random() * 320,
        Math.random() * 100,
        Math.random() * 100,
        {
          backgroundColor: Math.random() * 0xffffffff,
          borderColor: Math.random() * 0xffffffff,
        }
      )
    );
  }
  tabs.addTab("Rects", rects);

  const texts = new LCD.Scroll(0, 0, 480, 290);
  for (let i = 0; i < 10; i++) {
    texts.addContentChild(
      new LCD.Text(
        Math.random() * 480,
        Math.random() * 320,
        `Text${i}`,
        "Arial",
        {
          color: Math.random() * 0xffffffff,
          backgroundColor: Math.random() * 0xffffffff,
          borderColor: Math.random() * 0xffffffff,
        }
      )
    );
  }
  tabs.addTab("Texts", texts);

  const images = new LCD.Scroll(0, 0, 480, 290);
  for (let i = 0; i < 2; i++) {
    images.addContentChild(
      new LCD.Image(
        Math.random() * 480,
        Math.random() * 320,
        "res/img/asdf.png"
      )
    );
  }
  tabs.addTab("Images", images);

  let data = [
    new LCD.Text(0, 0, "Text1", "Arial"),
    new LCD.Text(0, 0, "Text2", "Arial"),
    new LCD.Text(0, 0, "Text3", "Arial"),
    new LCD.Text(0, 0, "Text4", "Arial"),
    new LCD.Text(0, 0, "Text5", "Arial"),
    new LCD.Text(0, 0, "Text6", "Arial"),
    new LCD.Text(0, 0, "Text7", "Arial"),
    new LCD.Text(0, 0, "Text8", "Arial"),
    new LCD.Text(0, 0, "Text9", "Arial"),
    new LCD.Text(0, 0, "Text10", "Arial"),
    new LCD.Text(0, 0, "Text11", "Arial"),
    new LCD.Text(0, 0, "Text12", "Arial"),
    new LCD.Text(0, 0, "Text13", "Arial"),
    new LCD.Text(0, 0, "Text14", "Arial"),
    new LCD.Text(0, 0, "Text15", "Arial"),
    new LCD.Text(0, 0, "Text16", "Arial"),
    new LCD.Text(0, 0, "Text17", "Arial"),
    new LCD.Text(0, 0, "Text18", "Arial"),
    new LCD.Text(0, 0, "Text19", "Arial"),
    new LCD.Text(0, 0, "Text20", "Arial"),
  ];
  const list = new LCD.List(0, 0, 480, 290, data);
  tabs.addTab("List", list);

  canvas.addChild(tabs);
});
