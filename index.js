const Button = require("./lib/components/Button");
const Canvas = require("./lib/components/Canvas");
const Circle = require("./lib/components/Circle");
const Component = require("./lib/components/Component");
const Scroll = require("./lib/components/Scroll");
const Rect = require("./lib/components/Rect");
const Text = require("./lib/components/Text");
const Selector = require("./lib/components/Selector");
const Tab = require("./lib/components/Tab");
const Image = require("./lib/components/Image");
const List = require("./lib/components/List");

const BMFont = require("./lib/draw/bmfont/BMFont");

const Context2d = require("./lib/draw/Context2d");

const ILI9486 = require("./lib/interface/impl/ILI9486");
const XPT2046 = require("./lib/interface/impl/XPT2046");

const LCD = require("./lib/interface/LCD");
const Rotation = require("./lib/interface/Rotation");
const Touchpad = require("./lib/interface/Touchpad");

const UI = require("./lib/UI");

module.exports = {
  Button,
  Canvas,
  Circle,
  Component,
  Scroll,
  Rect,
  Text,
  Selector,
  Tab,
  Image,
  List,
  BMFont,
  Context2d,
  ILI9486,
  XPT2046,
  LCD,
  Rotation,
  Touchpad,
  UI,
};
