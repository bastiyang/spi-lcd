function sleep(millis) {
  return new Promise((accept) => {
    setTimeout(accept, millis);
  });
}
module.exports = {
  sleep,
};
