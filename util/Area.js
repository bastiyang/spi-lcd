class Area {
  constructor(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  set(x1, y1, x2, y2) {
    this.x1 = x1;
    this.y1 = y1;
    this.x2 = x2;
    this.y2 = y2;
  }

  extend(x1, y1, x2, y2) {
    this.x1 = Math.min(this.x1, x1);
    this.y1 = Math.min(this.y1, y1);
    this.x2 = Math.max(this.x2, x2);
    this.y2 = Math.max(this.y2, y2);
  }
}
module.exports = Area;
