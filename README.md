# spi-lcd

NodeJS driver for SPI LCD development, like those 3.5 inch LCDs found on AliExpress

LCD Compatibility list:

- ILI9486 (Found on Waveshare 3.5 (C))

Touchpad compatibility list:

- XPT2046 (Found on Waveshare 3.5 (C))

Because of rpio library, which is faster than normal spi connection, this project has to be executed as ROOT for it to acces gpiomem.

Requirements:

- NodeJS 14.15.1

Test:
npm install
node test.js

As a lib:
npm install git+ssh://git@gitlab.com/bastiyang/spi-lcd.git
