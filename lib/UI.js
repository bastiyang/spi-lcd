const PNG = require("png-js");
const rpio = require("rpio");
const Canvas = require("./components/Canvas");
const BMFont = require("./draw/bmfont/BMFont");
const Rotation = require("./interface/Rotation");
const XPT2046 = require("./interface/impl/XPT2046");
const ILI9486 = require("./interface/impl/ILI9486");

const defaultOptions = {
  width: 480,
  height: 320,
  rotation: Rotation.Down,
};

class UI {
  options = defaultOptions;
  pngs = {};
  fonts = {};
  async initCanvas(fonts = [], pngs = [], options = defaultOptions) {
    this.options = options;

    for (let i = 0; i < pngs.length; i++) {
      const png = pngs[i];
      const data = await new Promise((accept) => {
        const image = PNG.load(png);
        image.decode((pixels) => {
          accept({
            width: image.width,
            height: image.height,
            pixelData: pixels,
          });
        });
      });
      this.pngs[png] = data;
    }

    for (let i = 0; i < fonts.length; i++) {
      const font = fonts[i];
      const fontData = await new Promise((accept) => {
        const bmfont = new BMFont(font, () => {
          accept(bmfont);
        });
      });
      this.fonts[fontData.info.face] = fontData;
    }

    rpio.init({
      gpiomem: false,
      mapping: "physical",
    });

    let touchpad = new XPT2046(options.width, options.height, options.rotation);
    let lcd = new ILI9486(options.width, options.height, options.rotation);
    const canvas = new Canvas(options.width, options.height, lcd);

    let touched = null;
    touchpad.addTouchStartListener(({ x, y }) => {
      const element = canvas.elementAtPoint(x, y);
      if (element) {
        const { localX, localY } = element.toLocalPoint(x, y);
        element.fire("touchStart", { x, y, localX, localY });
        touched = element;
      }
    });
    touchpad.addTouchEndListener(({ x, y }) => {
      if (touched) {
        const { localX, localY } = touched.toLocalPoint(x, y);
        touched.fire("touchEnd", { x, y, localX, localY });
      }
    });
    touchpad.addTouchMoveListener(({ x, y }) => {
      if (touched) {
        const { localX, localY } = touched.toLocalPoint(x, y);
        touched.fire("touchMove", { x, y, localX, localY });
      }
    });

    let interval = setInterval(() => {
      touchpad.update();
      canvas.update();
      canvas.draw();
    });

    process.on("SIGINT", (_) => {
      clearInterval(interval);
    });

    return canvas;
  }
}
module.exports = new UI();
