const Component = require("./Component");

class Rect extends Component {
  constructor(x, y, width, height, style) {
    super(x, y, width, height, style);
  }

  draw(ctx, mask) {
    const posX = this.getScreenPosX();
    const posY = this.getScreenPosY();

    const da = super.draw(ctx, mask);
    if (this.isVisible()) {
      ctx.fillRect(
        posX,
        posY,
        this.getWidth(),
        this.getHeight(),
        this.style.backgroundColor,
        mask
      );
      ctx.drawRect(
        posX,
        posY,
        this.getWidth(),
        this.getHeight(),
        this.style.borderColor,
        mask
      );
    }
    return da;
  }
}
module.exports = Rect;
