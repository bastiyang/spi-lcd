const Component = require("./Component");
const Scroll = require("./Scroll");

class List extends Component {
  currentSelected = null;
  newSelected = null;
  scroll = null;
  content = null;
  onChange = null;
  data = null;

  constructor(x, y, width, height, data) {
    super(x, y, width, height);

    this.setData(data);
  }

  setData(data) {
    if (data) {
      if (this.scroll) {
        this.removeChild(this.scroll);
      }

      this.scroll = new Scroll(0, 0, this.getWidth(), this.getHeight());
      let lastY = 0;
      for (let i = 0; i < data.length; i++) {
        const element = data[i];
        const content = new Component(
          0,
          lastY,
          this.getWidth() - 20,
          element.getHeight(),
          {
            backgroundColor: 0x000000ff,
          }
        );
        content.on("touchEnd", () => {
          this.newSelected = content;
        });
        lastY += element.getHeight();
        content.addChild(element);
        this.scroll.addContentChild(content);
      }

      this.addChild(this.scroll);
      this.data = data;
    }
  }

  update(dt) {
    super.update(dt);
    if (this.newSelected && this.newSelected != this.currentSelected) {
      if (this.currentSelected) {
        this.currentSelected.setStyle({
          backgroundColor: 0x000000ff,
        });
      }
      this.newSelected.setStyle({
        backgroundColor: 0x00ff00ff,
      });

      if (this.onChange) {
        this.onChange(
          this.newSelected,
          this.data.indexOf(this.newSelected.getChildAt(0))
        );
      }
      this.currentSelected = this.newSelected;
      this.newSelected = null;
    }
  }
}

module.exports = List;
