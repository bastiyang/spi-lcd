const Component = require("./Component");

class Circle extends Component {
  radius = 0;
  constructor(x, y, radius, style) {
    super(x, y, radius * 2, radius * 2, style);
    this.radius = radius;
  }

  draw(ctx, mask) {
    super.draw(ctx, mask, false);

    if (this.isVisible()) {
      const posX = this.getScreenPosX();
      const posY = this.getScreenPosY();

      ctx.fillCircle(posX, posY, this.radius, this.style.backgroundColor, mask);
      ctx.drawCircle(posX, posY, this.radius, this.style.borderColor, mask);
    }
  }

  hitTest(x, y) {
    let visible = this.isVisible();
    let parent = this.parent;
    while (parent) {
      visible = visible ? visible : false;
      parent = parent.parent;
    }
    if (visible) {
      const posX = this.getScreenPosX();
      const posY = this.getScreenPosY();
      const touchable =
        this.listeners["touchStart"].length > 0 ||
        this.listeners["touchEnd"].length > 0 ||
        this.listeners["touchMove"].length > 0;
      if (touchable) {
        const distance = Math.sqrt(
          Math.pow(x - posX - this.radius, 2) +
            Math.pow(y - posY - this.radius, 2)
        );
        return distance <= this.radius;
      }
    }
    return false;
  }
}
module.exports = Circle;
