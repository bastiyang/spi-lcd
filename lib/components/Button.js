const Component = require("./Component");
const Rect = require("./Rect");
const Text = require("./Text");

class Button extends Component {
  constructor(
    text,
    font,
    onPress,
    x,
    y,
    width,
    height,
    normalStyle = {
      backgroundColor: 0xff0000ff,
    },
    pressedStyle = {
      backgroundColor: 0xaa0000ff,
    }
  ) {
    let t = new Text(0, 0, text, font, {
      backgroundColor: 0x00000000,
      color: 0x00000000,
      borderColor: 0x00000000,
    });
    if (width === 0) {
      width = t.getWidth() + 10;
    }
    if (height === 0) {
      height = t.getHeight() + 10;
    }
    super(x, y, width, height);

    this.text = t;
    this.text.setPositionX(width / 2 - this.text.getWidth() / 2);
    this.text.setPositionY(height / 2 - this.text.getHeight() / 2);

    this.normal = new Rect(0, 0, width, height);
    this.normal.style = normalStyle;

    this.pressed = new Rect(0, 0, width, height);
    this.pressed.style = pressedStyle;
    this.pressed.setVisible(false);

    this.addChild(this.pressed);
    this.addChild(this.normal);
    this.addChild(this.text);

    this.on("touchStart", () => {
      this.pressed.setVisible(true);
      this.normal.setVisible(false);
    });

    this.on("touchEnd", () => {
      this.pressed.setVisible(false);
      this.normal.setVisible(true);
      if (onPress) {
        onPress();
      }
    });
  }
}
module.exports = Button;
