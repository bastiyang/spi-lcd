class Component {
  posX = 0;
  posY = 0;
  width = 0;
  height = 0;
  children = [];
  style = null;
  dirty = false;
  parent = null;
  tag = "";
  listeners = {
    touchStart: [],
    touchEnd: [],
    touchMove: [],
  };
  visible = true;
  lastPosition = null;

  constructor(
    x,
    y,
    width,
    height,
    style = {
      color: 0x00000000,
      backgroundColor: 0x00000000,
      borderColor: 0x00000000,
      overflow: "visible",
    }
  ) {
    this.width = width;
    this.height = height;
    this.posX = x;
    this.posY = y;
    this.dirty = true;
    this.style = style;
  }

  setTag(value) {
    this.tag = value;
  }

  getTag() {
    return this.tag;
  }

  setStyle(style) {
    this.setDirty(true);
    this.style = style;
  }

  setDirty(dirty) {
    if (dirty) {
      this.dirty = true;
      this.children.forEach((c) => {
        c.setDirty(true);
      });
    } else {
      this.dirty = false;
    }
  }

  setPosition(x, y) {
    this.setDirty(true);
    this.posX = parseInt(x);
    this.posY = parseInt(y);
  }

  setPositionX(x) {
    this.setDirty(true);
    this.posX = parseInt(x);
  }

  setPositionY(y) {
    this.setDirty(true);
    this.posY = parseInt(y);
  }

  setWidth(width) {
    this.setDirty(true);
    this.width = width;
  }

  getWidth() {
    return this.width;
  }

  setHeight(height) {
    this.setDirty(true);
    this.height = height;
  }

  getHeight() {
    return this.height;
  }

  setVisible(visible) {
    this.setDirty(true);
    this.visible = visible;
  }

  isVisible() {
    return this.visible;
  }

  countChildren() {
    return this.children.length;
  }

  getChildAt(index) {
    return this.children[index];
  }

  addChild(child) {
    this.setDirty(true);
    child.setDirty(true);
    child.parent = this;
    this.children.push(child);
  }

  removeChild(child) {
    this.setDirty(true);
    const index = this.children.indexOf(child);
    if (index >= 0) {
      this.children.splice(index, 1);
    }
  }

  update(dt) {
    this.children.forEach((c) => {
      c.update(dt);
    });
  }

  draw(ctx, mask, drawBGAndBorder = true) {
    if (this.lastPosition) {
      const { x1, y1, x2, y2 } = this.lastPosition;
      ctx.addRedrawArea(x1, y1, x2, y2);
    }
    if (this.isVisible()) {
      const posX = this.getScreenPosX();
      const posY = this.getScreenPosY();
      if (this.style.overflow === "hidden") {
        if (mask) {
          mask = {
            x: posX,
            y: posY,
            w: this.getWidth(),
            h: this.getHeight(),
          };
        } else {
          mask = {
            x: posX,
            y: posY,
            w: this.getWidth(),
            h: this.getHeight(),
          };
        }
      }
      if (this.dirty) {
        if (
          drawBGAndBorder &&
          this.style.backgroundColor &&
          this.style.backgroundColor !== 0x00000000
        ) {
          ctx.fillRect(
            posX,
            posY,
            this.getWidth(),
            this.getHeight(),
            this.style.backgroundColor,
            mask
          );
        }
        if (
          drawBGAndBorder &&
          this.style.borderColor &&
          this.style.borderColor !== 0x00000000
        ) {
          ctx.drawRect(
            posX,
            posY,
            this.getWidth(),
            this.getHeight(),
            this.style.borderColor,
            mask
          );
        }
        if (mask) {
          let x1 = Math.max(posX, mask.x);
          let y1 = Math.max(posY, mask.y);
          let x2 = Math.min(mask.w + mask.x, this.getWidth() + posX);
          let y2 = Math.min(mask.h + mask.y, this.getHeight() + posY);
          ctx.addRedrawArea(x1, y1, x2, y2);
          this.lastPosition = { x1, y1, x2, y2 };
        } else {
          let x1 = posX;
          let y1 = posY;
          let x2 = this.getWidth() + posX;
          let y2 = this.getHeight() + posY;
          ctx.addRedrawArea(x1, y1, x2, y2);
          this.lastPosition = { x1, y1, x2, y2 };
        }
        this.setDirty(false);
      }
      this.children.forEach((c) => {
        c.draw(ctx, mask);
      });
    }
  }

  getPositionX() {
    return this.posX;
  }

  getPositionY() {
    return this.posY;
  }

  getScreenPosX() {
    let pos = this.getPositionX();
    let parent = this.parent;
    while (parent) {
      pos += parent.getPositionX();
      parent = parent.parent;
    }
    return pos;
  }

  getScreenPosY() {
    let pos = this.getPositionY();
    let parent = this.parent;
    while (parent) {
      pos += parent.getPositionY();
      parent = parent.parent;
    }
    return pos;
  }

  toLocalPoint(x, y) {
    let localX = x;
    let localY = y;
    let parent = this.parent;
    while (parent) {
      localX -= parent.getPositionX();
      localY -= parent.getPositionY();
      parent = parent.parent;
    }
    return { localX, localY };
  }

  hitTest(x, y) {
    const posX = this.getScreenPosX();
    const posY = this.getScreenPosY();
    const touchable =
      this.listeners["touchStart"].length > 0 ||
      this.listeners["touchEnd"].length > 0 ||
      this.listeners["touchMove"].length > 0;
    if (touchable) {
      return (
        x >= posX &&
        x <= posX + this.getWidth() &&
        y >= posY &&
        y <= posY + this.getHeight()
      );
    }
    return false;
  }

  elementAtPoint(x, y) {
    if (this.isVisible()) {
      for (let i = this.children.length - 1; i >= 0; i--) {
        const child = this.children[i];
        const e = child.elementAtPoint(x, y);
        if (e) {
          return e;
        }
      }
      if (this.hitTest(x, y)) {
        return this;
      }
    }
  }

  on(eventName, event) {
    let listeners = this.listeners[eventName];
    if (listeners) {
      listeners.push(event);
    }
  }

  off(eventName, event) {
    let listeners = this.listeners[eventName];
    if (listeners) {
      listeners.splice(listeners.indexOf(event), 1);
    }
  }

  fire(eventName, event) {
    let listeners = this.listeners[eventName];
    if (listeners) {
      listeners.forEach((l) => {
        l(event);
      });
    }
  }
}
module.exports = Component;
