const Button = require("./Button");
const Component = require("./Component");

class Tab extends Component {
  currentTab = null;
  newTab = null;
  tabs = {};
  tabButtons = null;
  tabContent = null;

  constructor(x, y, width, height) {
    super(x, y, width, height);
    this.tabButtons = new Component(0, 0, width, 30);
    this.tabContent = new Component(0, 30, width, height - 30);

    this.addChild(this.tabButtons);
    this.addChild(this.tabContent);
  }

  addTab(tabName, component) {
    const lastButton = this.tabButtons.getChildAt(
      this.tabButtons.countChildren() - 1
    );

    this.tabButtons.addChild(
      new Button(
        tabName,
        "Arial",
        () => {
          this.newTab = tabName;
        },
        lastButton ? lastButton.getPositionX() + lastButton.getWidth() + 10 : 0,
        0,
        0,
        30
      )
    );

    if (!this.currentTab && !this.newTab) {
      this.newTab = tabName;
    }

    component.setVisible(false);
    this.tabs[tabName] = component;
    this.tabContent.addChild(component);
  }

  update(dt) {
    super.update(dt);
    if (this.newTab && this.currentTab !== this.newTab) {
      if (this.currentTab) {
        this.tabs[this.currentTab].setVisible(false);
      }
      this.tabs[this.newTab].setVisible(true);

      this.currentTab = this.newTab;
      this.newTab = null;
    }
  }
}

module.exports = Tab;
