const Button = require("./Button");
const Component = require("./Component");
const Text = require("./Text");

class Selector extends Component {
  currentIndex = -1;
  data = [];
  buttonLeft = null;
  buttonRight = null;
  text = null;
  valueField = null;
  titleField = null;

  onChange = null;

  constructor(data, x, y, width, height) {
    super(x, y, width, height);
    this.data = data;
    if (data.length > 0) {
      this.currentIndex = 0;
      this.buttonLeft = new Button(
        "<",
        "Arial",
        this.goLeft.bind(this),
        0,
        0,
        30,
        height
      );
      this.text = new Text(35, 0, "", "Arial");
      this.buttonRight = new Button(
        ">",
        "Arial",
        this.goRight.bind(this),
        width - 30,
        0,
        30,
        height
      );

      this.addChild(this.buttonLeft);
      this.addChild(this.text);
      this.addChild(this.buttonRight);
    }
  }

  setData(data) {
    this.setDirty(true);
    const values = Object.values(data);
    if (values.length > 0) {
      this.data = data;
      this.currentIndex = 0;
    }
  }

  setValueField(field) {
    this.setDirty(true);
    this.valueField = field;
  }

  setTitleField(field) {
    this.setDirty(true);
    this.titleField = field;
  }

  getData() {
    return this.data;
  }

  getSelected() {
    return this.data[this.currentIndex];
  }

  getSelectedTitle() {
    return this.titleField
      ? this.data[this.currentIndex][this.titleField]
      : this.data[this.currentIndex];
  }

  getSelectedValue() {
    return this.valueField
      ? this.data[this.currentIndex][this.valueField]
      : this.data[this.currentIndex];
  }

  setSelectedValue(selected) {
    if (this.valueField) {
      for (let i = 0; i < this.data.length; i++) {
        let value = this.data[i];
        if (value[this.valueField] === selected) {
          this.currentIndex = i;
          break;
        }
      }
    } else {
      this.currentIndex = this.data.indexOf(selected);
    }
  }

  goLeft() {
    if (this.data.length > 0) {
      this.currentIndex =
        (this.currentIndex - 1 + this.data.length) % this.data.length;
    }
  }

  goRight() {
    if (this.data.length > 0) {
      this.currentIndex =
        (this.currentIndex + 1 + this.data.length) % this.data.length;
    }
  }

  update() {
    const nextTitle = this.getSelectedTitle();
    if (nextTitle && nextTitle !== this.text.getText()) {
      this.text.setText(nextTitle);
      if (this.onChange) {
        this.onChange(this.getSelectedValue(), this.currentIndex);
      }
    }
  }
}

module.exports = Selector;
