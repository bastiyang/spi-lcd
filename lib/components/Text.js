const UI = require("../UI");
const Component = require("./Component");

class Text extends Component {
  text = null;
  font = null;
  textCtx = null;
  constructor(x, y, text, font, style) {
    const f = UI.fonts[font];
    const textCtx = f.getTextContext(text);
    super(x, y, textCtx.width, textCtx.height, style);
    this.text = text;
    this.font = font;
    this.textCtx = textCtx;
  }

  draw(ctx, mask) {
    const posX = this.getScreenPosX();
    const posY = this.getScreenPosY();

    const da = super.draw(ctx);
    if (this.isVisible()) {
      ctx.fillRect(
        posX,
        posY,
        this.getWidth(),
        this.getHeight(),
        this.style.backgroundColor,
        mask
      );
      ctx.drawRect(
        posX,
        posY,
        this.getWidth(),
        this.getHeight(),
        this.style.borderColor,
        mask
      );
      ctx.drawContext(posX, posY, this.textCtx, mask);
    }
    return da;
  }

  getText() {
    return this.text;
  }

  setText(text) {
    this.text = text;
    const f = UI.fonts[this.font];
    const textCtx = f.getTextContext(text);
    this.textCtx = textCtx;
    this.setWidth(textCtx.width);
    this.setHeight(textCtx.height);
  }
}
module.exports = Text;
