const Context2d = require("../draw/Context2d");
const Component = require("./Component");

class Canvas extends Component {
  ctx;
  currentTime = process.hrtime();

  constructor(width, height, output) {
    super(0, 0, width, height);
    this.output = output;
    this.ctx = new Context2d(this.getWidth(), this.getHeight());
  }

  update() {
    const current = this.currentTime;
    this.currentTime = process.hrtime();
    const secs = this.currentTime[0] - current[0];
    const nsecs = this.currentTime[1] - current[1];
    const dt = secs + nsecs / 1000000000;

    super.update(dt);
  }

  draw() {
    this.ctx.clear();
    super.draw(this.ctx);

    const area = this.ctx.getRedrawArea();
    if (area) {
      const x1 = Math.max(0, area.x1 - 1);
      const y1 = Math.max(0, area.y1 - 1);
      const x2 = Math.min(this.getWidth(), area.x2 + 1);
      const y2 = Math.min(this.getHeight(), area.y2 + 1);
      let w = x2 - x1;
      let h = y2 - y1;

      if (w > 0 && h > 0) {
        const buffer = this.ctx.copy565(x1, y1, x2, y2);
        this.output.writePixelData(x1, y1, w, h, buffer);
      }
    }
  }
}
module.exports = Canvas;
