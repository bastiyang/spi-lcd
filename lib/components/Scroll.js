const Component = require("./Component");
const Rect = require("./Rect");

class Scroll extends Component {
  scrollBarHorizontal = null;
  scrollBarVertical = null;
  content = null;
  constructor(x, y, width, height) {
    super(x, y, width, height);

    const scrollWidth = 20;
    const scrollHeight = 100;

    this.mask = new Component(0, 0, width - scrollWidth, height - scrollWidth);
    this.mask.style = {
      overflow: "hidden",
    };

    this.content = new Component(0, 0, 0, 0);

    this.scrollBarHorizontal = new Rect(
      0,
      height - scrollWidth,
      scrollHeight,
      scrollWidth
    );
    this.scrollBarHorizontal.style = {
      backgroundColor: 0xff0000ff,
    };
    this.scrollBarVertical = new Rect(
      width - scrollWidth,
      0,
      scrollWidth,
      scrollHeight
    );
    this.scrollBarVertical.style = {
      backgroundColor: 0xff0000ff,
    };

    this.mask.addChild(this.content);
    this.addChild(this.mask);
    this.addChild(this.scrollBarVertical);
    this.addChild(this.scrollBarHorizontal);

    this.scrollBarVertical.on("touchMove", (ev) => {
      const bothVisibleOffset =
        this.scrollBarHorizontal.isVisible() &&
        this.scrollBarVertical.isVisible()
          ? this.scrollBarHorizontal.getHeight()
          : 0;
      this.scrollBarVertical.setPositionY(
        Math.min(
          height - scrollHeight - bothVisibleOffset,
          Math.max(0, ev.localY)
        )
      );
    });

    this.scrollBarHorizontal.on("touchMove", (ev) => {
      const bothVisibleOffset =
        this.scrollBarHorizontal.isVisible() &&
        this.scrollBarVertical.isVisible()
          ? this.scrollBarHorizontal.getHeight()
          : 0;
      this.scrollBarHorizontal.setPositionX(
        Math.min(
          width - scrollHeight - bothVisibleOffset,
          Math.max(0, ev.localX)
        )
      );
    });
  }

  addContentChild(child) {
    this.content.addChild(child);

    const contentHeight = this.content.getHeight();
    const contentWidth = this.content.getWidth();

    this.content.setHeight(
      Math.max(contentHeight, child.getPositionY() + child.getHeight())
    );

    this.content.setWidth(
      Math.max(contentWidth, child.getPositionX() + child.getWidth())
    );
  }

  update(dt) {
    super.update(dt);
    const verticalVisible = this.content.getHeight() > this.getHeight();
    const horizontalVisible = this.content.getWidth() > this.getWidth();
    const bothVisibleOffset =
      verticalVisible && horizontalVisible
        ? this.scrollBarHorizontal.getHeight()
        : 0;

    if (verticalVisible) {
      this.scrollBarVertical.setVisible(true);
      const percentY =
        this.scrollBarVertical.getPositionY() /
        (this.getHeight() -
          this.scrollBarVertical.getHeight() -
          bothVisibleOffset);

      this.content.setPositionY(
        (this.getHeight() - bothVisibleOffset - this.content.getHeight()) *
          percentY
      );
      this.mask.setWidth(this.width - this.scrollBarVertical.getWidth());
    } else {
      this.scrollBarVertical.setVisible(false);
      this.mask.setWidth(this.width);
    }

    if (horizontalVisible) {
      this.scrollBarHorizontal.setVisible(true);
      const percentX =
        this.scrollBarHorizontal.getPositionX() /
        (this.getWidth() -
          this.scrollBarHorizontal.getWidth() -
          bothVisibleOffset);

      this.content.setPositionX(
        (this.getWidth() - bothVisibleOffset - this.content.getWidth()) *
          percentX
      );
      this.mask.setHeight(this.height - this.scrollBarVertical.getWidth());
    } else {
      this.scrollBarHorizontal.setVisible(false);
      this.mask.setHeight(this.height);
    }
  }
}
module.exports = Scroll;
