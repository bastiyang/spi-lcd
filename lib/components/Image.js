const Context2d = require("../draw/Context2d");
const UI = require("../UI");
const Component = require("./Component");

class Image extends Component {
  constructor(x, y, imageName) {
    const image = UI.pngs[imageName];
    super(x, y, image.width, image.height);
    this.ctx = new Context2d(image.width, image.height);
    this.ctx.drawRGBA(0, 0, image.width, image.height, image.pixelData);
  }

  draw(ctx, mask) {
    const posX = this.getScreenPosX();
    const posY = this.getScreenPosY();

    const da = super.draw(ctx);
    if (this.isVisible()) {
      ctx.fillRect(
        posX,
        posY,
        this.getWidth(),
        this.getHeight(),
        this.style.backgroundColor,
        mask
      );
      ctx.drawRect(
        posX,
        posY,
        this.getWidth(),
        this.getHeight(),
        this.style.borderColor,
        mask
      );
      ctx.drawContext(posX, posY, this.ctx, mask);
    }
    return da;
  }
}
module.exports = Image;
