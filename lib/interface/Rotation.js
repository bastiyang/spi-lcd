const Rotation = {
  Up: "Up",
  Down: "Down",
  Left: "Left",
  Right: "Right",
};
module.exports = Rotation;
