const Rotation = require("./Rotation");

class Touchpad {
  constructor(width, height, rotation, minRawX, maxRawX, minRawY, maxRawY) {
    this.width = width;
    this.height = height;
    this.rotation = rotation;
    this.minRawX = minRawX;
    this.minRawY = minRawY;
    this.maxRawX = maxRawX;
    this.maxRawY = maxRawY;

    this.isTouching = false;
    this.isMoving = false;
    this.x = 0;
    this.y = 0;

    this.touchMoveEvents = [];
    this.touchStartEvents = [];
    this.touchEndEvents = [];
  }

  setDimensions(width, height) {
    this.width = width;
    this.height = height;
  }

  update() {
    const { rawX, rawY, rawZ } = this.rawXYZ();

    if (rawZ > 0) {
      let currentRawX,
        currentRawY,
        newX = this.x,
        newY = this.y;
      currentRawX =
        (Math.min(Math.max(rawX, this.minRawX), this.maxRawX) - this.minRawX) /
        (this.maxRawX - this.minRawX);
      currentRawY =
        (Math.min(Math.max(rawY, this.minRawY), this.maxRawY) - this.minRawY) /
        (this.maxRawY - this.minRawY);
      switch (this.rotation) {
        default:
        case Rotation.Up: {
          newX = this.width * currentRawY;
          newY = this.height * (1 - currentRawX);
          break;
        }
        case Rotation.Down: {
          newX = this.width * (1 - currentRawY);
          newY = this.height * currentRawX;
          break;
        }
        case Rotation.Left: {
          newX = currentRawY * this.height;
          newY = currentRawX * this.width;
          break;
        }
        case Rotation.Right: {
          newX = this.height - currentRawY * this.height;
          newY = this.width - currentRawX * this.width;
          break;
        }
      }
      newX = Math.round(newX);
      newY = Math.round(newY);
      if (this.x !== newX || this.y !== newY) {
        this.x = newX;
        this.y = newY;
        if (!this.isTouching) {
          this.isTouching = true;
          this.fireTouchStart(this.x, this.y);
        } else {
          this.isMoving = true;
          this.fireTouchMove(this.x, this.y);
        }
      } else {
        this.isMoving = false;
      }
    } else {
      if (this.isTouching) {
        this.isTouching = false;
        this.fireTouchEnd(this.x, this.y);
      }
    }
  }

  write(value) {}

  rawXYZ() {
    return { rawX: 0, rawY: 0, rawZ: 0 };
  }

  addTouchMoveListener(listener) {
    this.touchMoveEvents.push(listener);
  }
  removeTouchMoveListener(listener) {
    this.touchMoveEvents.splice(this.touchMoveEvents.indexOf(listener), 1);
  }
  fireTouchMove(x, y) {
    this.touchMoveEvents.forEach((e) => {
      if (e) e({ x, y });
    });
  }

  addTouchStartListener(listener) {
    this.touchStartEvents.push(listener);
  }
  removeTouchStartListener(listener) {
    this.touchStartEvents.splice(this.touchStartEvents.indexOf(listener), 1);
  }
  fireTouchStart(x, y) {
    this.touchStartEvents.forEach((e) => {
      if (e) e({ x, y });
    });
  }

  addTouchEndListener(listener) {
    this.touchEndEvents.push(listener);
  }
  removeTouchStartListener(listener) {
    this.touchEndEvents.splice(this.touchEndEvents.indexOf(listener), 1);
  }
  fireTouchEnd(x, y) {
    this.touchEndEvents.forEach((e) => {
      if (e) e({ x, y });
    });
  }

  close() {}
}
module.exports = Touchpad;
