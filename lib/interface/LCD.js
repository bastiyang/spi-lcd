class LCD {
  constructor(width, height, rotation) {
    this.width = width;
    this.height = height;
    this.rotation = rotation;
  }
}
module.exports = LCD;
