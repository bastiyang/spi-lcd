const rpio = require("rpio");
const Touchpad = require("../Touchpad");

const DIVIDER = 640;
const CHIP = 1;
const MIN_X = 2200;
const MAX_X = 25000;
const MIN_Y = 1000;
const MAX_Y = 29000;
const MIN_Z = 4000;
const MAX_z = 32000;

const S = 0b10000000;
const A2 = 0b01000000;
const A1 = 0b00100000;
const A0 = 0b00010000;
const MODE = 0b00001000;
const SER_DFR = 0b00000100;
const PD1 = 0b00000010;
const PD0 = 0b00000001;

class XPT2046 extends Touchpad {
  constructor(width, height, rotation) {
    super(width, height, rotation, MIN_X, MAX_X, MIN_Y, MAX_Y);
  }

  rawXYZ() {
    const base = S | PD0;
    const yPos = this.write(base | A0);
    const xPos = this.write(base | A0 | A2);
    const z1Pos = this.write(base | A0 | A1);
    const z2Pos = this.write(base | A2);
    return {
      rawX: xPos,
      rawY: yPos,
      rawZ: z1Pos,
    };
  }

  write(value) {
    rpio.spiBegin();
    rpio.spiChipSelect(CHIP);
    rpio.spiSetClockDivider(DIVIDER);

    const tx = Buffer.from([value, 0x00, 0x00]);
    const rx = Buffer.alloc(3);

    rpio.spiTransfer(tx, rx, 3);
    rpio.spiEnd();

    const response = (rx[1] << 8) + rx[2];
    return response;
  }
}
module.exports = XPT2046;
