const rpio = require("rpio");
const LCD = require("../LCD");
const Rotation = require("../Rotation");

const DIVIDER = 4;
const CHIP = 0;
const pins = {
  RESET: { pin: 22, mode: rpio.OUTPUT },
  RS: { pin: 18, mode: rpio.OUTPUT },
};

class ILI9486 extends LCD {
  constructor(width, height, rotation) {
    super(width, height, rotation);

    for (const k in pins) {
      const pin = pins[k];
      rpio.open(pin.pin, pin.mode);
    }

    rpio.spiSetCSPolarity(CHIP, 0);
    rpio.write(pins.RESET.pin, 1);
    rpio.msleep(5);
    rpio.write(pins.RESET.pin, 0);
    rpio.msleep(5);
    rpio.write(pins.RESET.pin, 1);
    rpio.msleep(5);

    let r;
    switch (rotation) {
      case Rotation.Up:
        r = 0b11100000;
        break;
      case Rotation.Down:
        r = 0b00100000;
        break;
      case Rotation.Left:
        r = 0b10000000;
        break;
      default:
        r = 0b01000000;
        break;
    }

    /* prettier-ignore */
    const init = [
      0x1000036, r,
      0x10000b4, 0x00,
      0x10000c1, 0x41,
      0x10000c5, 0x00, 0x91, 0x80, 0x00,
      0x10000e0, 0x0f, 0x1f, 0x1c, 0x0c, 0x0f, 0x08, 0x48, 0x98, 0x37, 0x0a, 0x13, 0x04, 0x11, 0x0d, 0x00,
      0x10000e1, 0x0f, 0x32, 0x2e, 0x0b, 0x0d, 0x05, 0x47, 0x75, 0x37, 0x06, 0x10, 0x03, 0x24, 0x20, 0x00,
      0x100003a, 0x55,
      0x1000011,
      0x20000ff,
      0x1000029,
    ];

    for (let i = 0; i < init.length; i++) {
      let value = init[i];
      if ((value & 0x1000000) === 0x1000000) {
        this.cmd(value & 0xff);
      } else if ((value & 0x2000000) == 0x2000000) {
        rpio.msleep(value & 0xff);
      } else {
        this.write([value & 0xff]);
      }
    }
  }

  cmd(command) {
    rpio.spiBegin();
    rpio.spiChipSelect(CHIP);
    rpio.spiSetClockDivider(DIVIDER);
    rpio.spiSetDataMode(0);
    rpio.write(pins.RS.pin, 0);
    rpio.spiSetCSPolarity(CHIP, 0);
    const tx = Buffer.from([command >> 8, command]);
    rpio.spiWrite(tx, tx.length);
    rpio.spiSetCSPolarity(CHIP, 1);
    rpio.spiEnd();
  }

  write(data) {
    rpio.spiBegin();
    rpio.spiChipSelect(CHIP);
    rpio.spiSetClockDivider(DIVIDER);
    rpio.spiSetDataMode(0);
    const buffer = Buffer.alloc(data.length * 2);
    for (let i = 0; i < data.length; i++) {
      let value = data[i];
      buffer[i * 2] = value >> 8;
      buffer[i * 2 + 1] = value & 0xff;
    }
    rpio.write(pins.RS.pin, 1);
    rpio.spiSetCSPolarity(CHIP, 0);
    const tx = buffer;
    rpio.spiWrite(tx, tx.length);
    rpio.spiSetCSPolarity(CHIP, 1);
    rpio.spiEnd();
  }

  writeBuffer(data) {
    rpio.spiBegin();
    rpio.spiChipSelect(CHIP);
    rpio.spiSetClockDivider(DIVIDER);
    rpio.spiSetDataMode(0);
    rpio.write(pins.RS.pin, 1);
    rpio.spiSetCSPolarity(CHIP, 0);
    const tx = data;
    rpio.spiWrite(tx, tx.length);
    rpio.spiSetCSPolarity(CHIP, 1);
    rpio.spiEnd();
  }

  writePixelData(x, y, w, h, buffer) {
    if (x >= 0 && y >= 0 && w > 0 && h > 0) {
      this.cmd(0x2a);
      this.write([x >> 8, x, (x + w - 1) >> 8, x + w - 1]);
      this.cmd(0x2b);
      this.write([y >> 8, y, (y + h - 1) >> 8, y + h - 1]);
      this.cmd(0x2c);
      this.writeBuffer(buffer);
    }
  }
}
module.exports = ILI9486;
