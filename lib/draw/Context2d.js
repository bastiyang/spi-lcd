const Area = require("../../util/Area");

function convertTo565(r, g, b, a) {
  r = r >> 3;
  g = g >> 2;
  b = b >> 3;
  return (b << 11) | (g << 5) | r;
}

function convertTo666(r, g, b, a) {
  r = r >> 2;
  g = g >> 2;
  b = b >> 2;
  return (b << 12) | (g << 6) | r;
}

class Context2d {
  buffer;
  oldRedrawArea = null;
  redrawArea = null;

  constructor(width, height) {
    this.width = width;
    this.height = height;
    this.buffer = Buffer.alloc(width * height * 4);
  }

  addRedrawArea(x1, y1, x2, y2) {
    if (!this.redrawArea) {
      this.redrawArea = new Area(x1, y1, x2, y2);
    } else {
      this.redrawArea.extend(x1, y1, x2, y2);
    }
  }

  getRedrawArea() {
    if (this.redrawArea) {
      const next = new Area(
        this.redrawArea.x1,
        this.redrawArea.y1,
        this.redrawArea.x2,
        this.redrawArea.y2
      );
      if (this.oldRedrawArea) {
        next.extend(
          this.oldRedrawArea.x1,
          this.oldRedrawArea.y1,
          this.oldRedrawArea.x2,
          this.oldRedrawArea.y2
        );
      }
      this.oldRedrawArea = this.redrawArea;
      this.redrawArea = null;
      return next;
    }

    return null;
  }

  getBuffer() {
    return this.buffer;
  }

  getBuffer16() {
    const buffer = this.copyBuffer(0, 0, this.width, this.height);
    return buffer;
  }

  copy(bufferX, bufferY, width, height) {
    const buffer = Buffer.alloc(width * height * 4);
    let i = 0;
    for (let y = 0; y < height; y++) {
      for (let x = 0; x < width; x++) {
        const index = (x + bufferX + (y + bufferY) * this.width) * 4;
        buffer[i++] = this.buffer[index];
        buffer[i++] = this.buffer[index + 1];
        buffer[i++] = this.buffer[index + 2];
        buffer[i++] = this.buffer[index + 3];
      }
    }
    return buffer;
  }

  copy565(x0, y0, x1, y1) {
    const w = x1 - x0;
    const h = y1 - y0;
    const buffer = Buffer.alloc(w * h * 2);
    let i = 0;

    for (let y = 0; y < h; y++) {
      for (let x = 0; x < w; x++) {
        const index = (x + x0 + (y + y0) * this.width) * 4;
        const color = convertTo565(
          this.buffer[index],
          this.buffer[index + 1],
          this.buffer[index + 2],
          this.buffer[index + 3]
        );
        buffer[i++] = (color >> 8) & 0xff;
        buffer[i++] = color & 0xff;
      }
    }
    return buffer;
  }

  copy666(x0, y0, x1, y1) {
    const w = x1 - x0;
    const h = y1 - y0;
    const buffer = Buffer.alloc(w * h * 2);
    let i = 0;

    for (let y = 0; y < h; y++) {
      for (let x = 0; x < w; x++) {
        const index = (x + x0 + (y + y0) * this.width) * 4;
        const color = convertTo666(
          this.buffer[index],
          this.buffer[index + 1],
          this.buffer[index + 2],
          this.buffer[index + 3]
        );
        buffer[i++] = (color >> 8) & 0xff;
        buffer[i++] = color & 0xff;
      }
    }
    return buffer;
  }

  drawPixel(x, y, color, mask) {
    x = parseInt(x);
    y = parseInt(y);
    let minX = mask ? Math.max(mask.x, 0) : 0;
    let maxX = mask ? Math.min(mask.x + mask.w, this.width) : this.width;
    let minY = mask ? Math.max(mask.y, 0) : 0;
    let maxY = mask ? Math.min(mask.y + mask.h, this.height) : this.height;

    if (x >= minX && x < maxX && y >= minY && y < maxY) {
      const index = (this.width * y + x) * 4;
      let rB = this.buffer[index];
      let gB = this.buffer[index + 1];
      let bB = this.buffer[index + 2];
      let aB = this.buffer[index + 3];

      let rA = (color >> 24) & 0xff;
      let gA = (color >> 16) & 0xff;
      let bA = (color >> 8) & 0xff;
      let aA = color & 0xff;

      const aOut = aA + (aB * (255 - aA)) / 255;
      if (aOut > 0) {
        const rOut = (rA * aA + (rB * aB * (255 - aA)) / 255) / aOut;
        const gOut = (gA * aA + (gB * aB * (255 - aA)) / 255) / aOut;
        const bOut = (bA * aA + (bB * aB * (255 - aA)) / 255) / aOut;
        this.buffer[index] = rOut;
        this.buffer[index + 1] = gOut;
        this.buffer[index + 2] = bOut;
        this.buffer[index + 3] = aOut;
      }
    }
  }

  drawLine(x1, y1, x2, y2, color, mask) {
    let dx = x2 - x1;
    let dy = y2 - y1;

    if (Math.abs(dx) > Math.abs(dy)) {
      for (let x = 0; dx >= 0 ? x <= dx : x >= dx; x += dx >= 0 ? 1 : -1) {
        this.drawPixel(x1 + x, parseInt(y1 + dy * (x / dx)), color, mask);
      }
    } else {
      for (let y = 0; dy >= 0 ? y <= dy : y >= dy; y += dy >= 0 ? 1 : -1) {
        this.drawPixel(parseInt(x1 + dx * (y / dy)), y1 + y, color, mask);
      }
    }
  }

  drawRect(x, y, w, h, color, mask) {
    const x1 = x + w;
    const y1 = y + h;
    this.drawLine(x, y, x1, y, color, mask);
    this.drawLine(x, y1, x1, y1, color, mask);
    this.drawLine(x, y, x, y1, color, mask);
    this.drawLine(x1, y, x1, y1, color, mask);
  }

  fillRect(x1, y1, w, h, color, mask) {
    const x2 = x1 + w;
    const y2 = y1 + h;
    const dx = x2 - x1;
    const dy = y2 - y1;
    for (let x = x1; dx >= 0 ? x <= x2 : x >= x2; x += dx >= 0 ? 1 : -1) {
      for (let y = y1; dy >= 0 ? y <= y2 : y >= y2; y += dy >= 0 ? 1 : -1) {
        this.drawPixel(x, y, color, mask);
      }
    }
  }

  drawCircle(cx, cy, r, color, mask) {
    cx += r;
    cy += r;
    let x = r;
    let y = 0;
    if (r > 0) {
      this.drawPixel(x + cx, y + cy, color, mask);
      this.drawPixel(-x + cx, y + cy, color, mask);
      this.drawPixel(y + cx, -x + cy, color, mask);
      this.drawPixel(-y + cx, x + cy, color, mask);
    }

    let p = 1 - r;
    while (x > y) {
      y++;
      if (p <= 0) {
        p = p + 2 * y + 1;
      } else {
        x--;
        p = p + 2 * y - 2 * x + 1;
      }
      if (x < y) {
        break;
      }
      this.drawPixel(x + cx, y + cy, color, mask);
      this.drawPixel(-x + cx, y + cy, color, mask);
      this.drawPixel(x + cx, -y + cy, color, mask);
      this.drawPixel(-x + cx, -y + cy, color, mask);

      if (x != y) {
        this.drawPixel(y + cx, x + cy, color, mask);
        this.drawPixel(-y + cx, x + cy, color, mask);
        this.drawPixel(y + cx, -x + cy, color, mask);
        this.drawPixel(-y + cx, -x + cy, color, mask);
      }
    }
  }

  fillCircle(x, y, r, color, mask) {
    x += r;
    y += r;
    for (let nx = -r; nx <= r; nx++) {
      let height = parseInt(Math.sqrt(r * r - nx * nx)) + 1;
      for (let ny = -height; ny < height; ny++) {
        this.drawPixel(nx + x, ny + y, color, mask);
      }
    }
  }

  drawContext(x, y, ctx, mask) {
    this.drawRGBA(x, y, ctx.width, ctx.height, ctx.getBuffer(), mask);
  }

  drawRGBA(x, y, width, height, data, mask) {
    for (let cy = 0; cy < height; cy++) {
      for (let cx = 0; cx < width; cx++) {
        const rgbaIndex = (cy * width + cx) * 4;
        const color =
          (data[rgbaIndex] << 24) |
          (data[rgbaIndex + 1] << 16) |
          (data[rgbaIndex + 2] << 8) |
          data[rgbaIndex + 3];
        this.drawPixel(x + cx, y + cy, color, mask);
      }
    }
  }

  appendRight(ctx, offsetX, offsetY, mask) {
    const buffer = this.buffer;
    const currentWidth = this.width;
    this.width += ctx.width;
    this.height = Math.max(this.height, ctx.height);
    this.buffer = Buffer.alloc(this.width * this.height * 4);

    this.drawRGBA(0, 0, currentWidth, this.height, buffer, mask);
    this.drawRGBA(
      currentWidth + offsetX,
      offsetY,
      ctx.width,
      ctx.height,
      ctx.getBuffer(),
      mask
    );
  }

  clear() {
    this.buffer.fill(0);
  }
}
module.exports = Context2d;
