const fs = require("fs");
const PNG = require("png-js");
const Context2d = require("../Context2d");

class BMFont {
  info = {};
  common = {};
  page = {};
  char = {};
  kerning = {};
  fontPNG = null;

  constructor(fileName, loadCallback) {
    const data = fs.readFileSync(fileName);
    const strData = `${data}`;

    const lines = strData.split("\n");
    lines.forEach((line) => {
      const l = line.trim();
      const entryName = l.substr(0, l.indexOf(" "));
      switch (entryName) {
        case "info": {
          const keyValuePair = l.split(" ");
          keyValuePair.splice(0, 1);
          keyValuePair.forEach((v) => {
            let keyValue = v.split("=");
            if (keyValue[1].indexOf('"') >= 0) {
              this.info[keyValue[0]] = keyValue[1]
                .replace('"', "")
                .replace('"', "");
            } else {
              this.info[keyValue[0]] = parseInt(keyValue[1]);
            }
          });
          break;
        }
        case "common": {
          const keyValuePair = l.split(" ");
          keyValuePair.splice(0, 1);
          keyValuePair.forEach((v) => {
            let keyValue = v.split("=");
            if (keyValue[1].indexOf('"') >= 0) {
              this.common[keyValue[0]] = keyValue[1]
                .replace('"', "")
                .replace('"', "");
            } else {
              this.common[keyValue[0]] = parseInt(keyValue[1]);
            }
          });
          break;
        }
        case "page": {
          const keyValuePair = l.split(" ");
          keyValuePair.splice(0, 1);
          keyValuePair.forEach((v) => {
            let keyValue = v.split("=");
            if (keyValue[1].indexOf('"') >= 0) {
              this.page[keyValue[0]] = keyValue[1]
                .replace('"', "")
                .replace('"', "");
            } else {
              this.info[keyValue[0]] = parseInt(keyValue[1]);
            }
          });
          break;
        }
        case "char": {
          const keyValuePair = l.replace(/\s\s+/g, " ").split(" ");
          keyValuePair.splice(0, 1);
          let charData = {};
          keyValuePair.forEach((v) => {
            let keyValue = v.split("=");
            if (keyValue[0] === "id") {
              this.char[String.fromCharCode(keyValue[1])] = charData;
            } else {
              charData[keyValue[0]] = parseInt(keyValue[1]);
            }
          });
          break;
        }
        case "kerning": {
          const keyValuePair = l.replace(/\s\s+/g, " ").split(" ");
          keyValuePair.splice(0, 1);
          let kerningData = {};
          let first = null;
          let second = null;
          keyValuePair.forEach((v) => {
            let keyValue = v.split("=");
            if (keyValue[0] === "first") {
              first = keyValue[1];
            } else if (keyValue[0] === "second") {
              second = keyValue[1];
            } else {
              kerningData[keyValue[0]] = parseInt(keyValue[1]);
            }
            if (first && second) {
              this.kerning[
                `${String.fromCharCode(first)}${String.fromCharCode(second)}`
              ] = kerningData;
            }
          });
          break;
        }
      }
    });
    const folder = fileName.substr(0, fileName.lastIndexOf("/"));
    const image = PNG.load(
      `${folder}${folder !== "" ? "/" : ""}${this.page.file}`
    );
    this.fontPNG = {
      width: image.width,
      height: image.height,
      pixels: null,
    };
    image.decode((pixels) => {
      for (let i = 0; i < pixels.length; i += 4) {
        let r = pixels[i];
        let g = pixels[i + 1];
        let b = pixels[i + 2];
        let a = pixels[i + 3];
        if (
          r === this.common.redChnl &&
          g === this.common.greenChnl &&
          b === this.common.blueChnl &&
          a === parseInt(this.common.alphaChnl * 0xff)
        ) {
          pixels[i] = 0;
          pixels[i + 1] = 0;
          pixels[i + 2] = 0;
          pixels[i + 3] = 0;
        }
      }
      this.fontPNG.pixels = pixels;
      loadCallback();
    });
  }

  getTextContext(text) {
    const ctx = new Context2d(0, 0);
    for (let i = 0; i < text.length; i++) {
      let offsetX = 0;
      if (i > 0) {
        const key = `${text[i - 1]}${text[i]}`;
        const kerning = this.kerning[key];
        if (kerning) {
          offsetX = kerning.amount;
        }
      }
      ctx.appendRight(this.getChar(text[i]), offsetX, 0);
    }

    return ctx;
  }

  getChar(char) {
    const { common } = this;
    const charData = this.char[char];
    const { x, y, width, height, xoffset, yoffset, xadvance } = charData;
    const png = this.fontPNG;

    const ctx = new Context2d(xadvance, common.lineHeight);
    for (let iy = 0; iy < height; iy++) {
      for (let ix = 0; ix < width; ix++) {
        let index = (ix + x + (iy + y) * png.width) * 4;
        ctx.drawRGBA(ix + xoffset, iy + yoffset, 1, 1, [
          png.pixels[index],
          png.pixels[index + 1],
          png.pixels[index + 2],
          png.pixels[index + 3],
        ]);
      }
    }

    return ctx;
  }
}
module.exports = BMFont;
